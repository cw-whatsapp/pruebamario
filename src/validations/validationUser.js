const validateNombre = (name) => {
    const nombreRegex = /^[a-zA-Z\s]{2,100}$/;

    if (!nombreRegex.test(name)) {
        return "El Nombre no es válido. Solo letras y espacios, y tenga una longitud entre 2 y 100 caracteres.";
    }
    return null;
};


const validateEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@(gmail\.com|hotmail\.com|yahoo\.es)$/i;

    if (!emailRegex.test(email)) {
        return "La dirección de correo electrónico no es válida o no utiliza un dominio permitido.";
    }

    if (email.length < 5 || email.length > 100) {
        return "La longitud del correo electrónico debe estar entre 5 y 100 caracteres.";
    }

    return null;
};

const validateContrasena = (password) => {
    if (!password || password.length < 8 || !/\d/.test(password) || !/[A-Z]/.test(password) || !/[a-z]/.test(password) || !/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(password)) {
        return "La contraseña debe tener al menos 8 caracteres, incluyendo números, letras mayúsculas, letras minúsculas y caracteres especiales.";
    }
    return null;
};


const validateIdUser = (idUser) => {
    if (idUser === "") {
        return "El ID  no puede estar vacío.";
    }
    if (!Number.isInteger(parseInt(idUser))) {
        return "El ID  debe ser un número entero.";
    }
    return null;
};





module.exports = {
    validateNombre,
    validateEmail,
    validateContrasena,
    validateIdUser
};
