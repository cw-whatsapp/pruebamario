const axios = require('axios');
require('dotenv').config();

const translateFields = (data) => {
    function cambiarLlaves(data) {
        return data.results.map(personaje => {
            return {
                nombre: personaje.name,
                altura: personaje.height,
                masa: personaje.mass,
                color_de_cabello: personaje.hair_color,
                color_de_piel: personaje.skin_color,
                color_de_ojos: personaje.eye_color,
                año_de_nacimiento: personaje.birth_year,
                genero: personaje.gender,
                planeta_natal: personaje.homeworld,
                peliculas: personaje.films,
                especies: personaje.species,
                vehiculos: personaje.vehicles,
                naves_estelares: personaje.starships,
                creado: personaje.created,
                editado: personaje.edited,
                url: personaje.url
            };
        });
    }


    const newData = {
        ...data,
        results: cambiarLlaves(data)
    };

    

    return newData;
};


const translateFieldsId = (data) => {
    const newData = {
        nombre: data.name,
        altura: data.height,
        masa: data.mass,
        color_de_cabello: data.hair_color,
        color_de_piel: data.skin_color,
        color_de_ojos: data.eye_color,
        año_de_nacimiento: data.birth_year,
        genero: data.gender,
        planeta_natal: data.homeworld,
        peliculas: data.films,
        especies: data.species,
        vehiculos: data.vehicles,
        naves_estelares: data.starships,
        creado: data.created,
        editado: data.edited,
        url: data.url
    };

    

    return newData;
};



const apiPeopleAll = async () => {
    try {
        const config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.SWAPI + 'people',
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const responseData = await axios.request(config);

        if (responseData.status === 200) {
            const translatedData = translateFields(responseData.data);
            return translatedData;
        } else {
            return 'La solicitud a la API falló';
        }
    } catch (error) {
        throw error;
    }
};

const apiPeopleId = async (id) => {
    
    try {
        const config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: process.env.SWAPI + 'people/' + id+"/",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const responseData = await axios.request(config);

        if (responseData.status === 200) {
            const translatedData = translateFieldsId(responseData.data);
            return translatedData;
        } else {
            return 'La solicitud a la API falló';
        }
    } catch (error) {
        throw error;
    }
};

module.exports = {
    apiPeopleAll, apiPeopleId,
};
