'use strict';

const { User } = require('./src/models/modelUser');
const { validateContrasena, validateNombre, validateEmail, validateIdUser } = require('./src/validations/validationUser');
const { apiPeopleAll, apiPeopleId } = require('./src/swapiPeople/peopleService');



const VALIDATION_ERROR_MESSAGE = 'Error de validación';
const NOT_FOUND_MESSAGE = 'El Usuario no se encuentra registrado.';
const _SUCCESS_MESSAGE = 'Usuario encontrado .';

const HTTP_BAD_REQUEST = 400;
const HTTP_CREATED = 201;


module.exports.registerUser = async (event, context, callback) => {
  try {

    const requestBody = JSON.parse(event.body);

    const dataToValidate = {
      nombre: requestBody.nombre,
      correo_electronico: requestBody.correo_electronico,
      contrasena: requestBody.contrasena,
      fecha_creacion: requestBody.fecha_creacion,
    };

    const validationErrors = [
      validateNombre(dataToValidate.nombre),
      validateEmail(dataToValidate.correo_electronico),
      validateContrasena(dataToValidate.contrasena),
    ].filter(error => error !== null);


    if (validationErrors.length > 0) {
      const response = {
        statusCode: 400,
        body: JSON.stringify({
          error: "Error de validación",
          errors: validationErrors,
        }),
      };

      return callback(null, response);
    }

    try {

      
      const existingUser = await User.findOne({
        where: {
          correo_electronico: dataToValidate.correo_electronico,
        },
      });

      if (existingUser) {
        const response = {
          statusCode: 400,
          body: JSON.stringify({
            error: "El correo electrónico ya está en uso.",
          }),
        };
        return callback(null, response);
      }


      const newUser = await User.create(dataToValidate);



      const response = {
        statusCode: 200,
        body: JSON.stringify(
          {
            message: 'usuario resgistrado',

          }
        ),
      };

      return callback(null, response);
    } catch (error) {
      const response = {
        statusCode: 400,
        body: error
      };
      return callback(null, response);

    }

  } catch (ex) {

    return callback(null, ex);
  }


};

module.exports.getUserAll = async (event, context, callback) => {
  try {

    const foundUser = await User.findAll({
      attributes: ['id', 'nombre', 'correo_electronico', 'contrasena', 'fecha_creacion'],
    });

    if (foundUser != null) {
      const response = {
        statusCode: HTTP_CREATED,
        body: JSON.stringify({
          message: _SUCCESS_MESSAGE,
          users: foundUser,
        }),
      };

      return callback(null, response);
    }

    const response = {
      statusCode: HTTP_CREATED,
      body: JSON.stringify({
        message: "No existe registros",
      }),
    };

    return callback(null, response);

  } catch (ex) {

    return callback(null, ex);
  }


};

module.exports.getUserId = async (event, context, callback) => {
  try {
    const { id } = JSON.parse(event.body);




    const dataToValidate = {
      idUser: id,
    };

    const errors = [validateIdUser(dataToValidate.idUser)].filter(error => error !== null);


    if (errors.length > 0) {
      const response = {
        statusCode: HTTP_BAD_REQUEST,
        body: JSON.stringify({
          error: VALIDATION_ERROR_MESSAGE,
          errors: errors,
        }),
      };

      return callback(null, response);
    }


    const foundUser = await User.findOne({
      where: {
        id: id,
      },
      attributes: ['id', 'nombre', 'correo_electronico', 'contrasena', 'fecha_creacion'],
    });




    if (foundUser != null) {
      const response = {
        statusCode: HTTP_CREATED,
        body: JSON.stringify({
          message: _SUCCESS_MESSAGE,
          user: foundUser,
        }),
      };

      return callback(null, response);
    }


    const response = {
      statusCode: HTTP_CREATED,
      body: JSON.stringify({
        message: NOT_FOUND_MESSAGE,
      }),
    };

    return callback(null, response);

  } catch (ex) {

    return callback(null, ex);
  }


};

module.exports.getSwapiPeopleAll = async (event, context, callback) => {
  try {

    const responseData = await apiPeopleAll();

    const response = {
      statusCode: HTTP_CREATED,
      body: JSON.stringify({
        data: responseData,
      }),
    };

    return callback(null, response);

  } catch (ex) {

    return callback(null, ex);
  }


};

module.exports.getSwapiPeopleId = async (event, context, callback) => {
  try {

    const { id } = JSON.parse(event.body);

    const dataToValidate = {
      idUser: id,
    };

    const errors = [validateIdUser(dataToValidate.idUser)].filter(error => error !== null);


    if (errors.length > 0) {
      const response = {
        statusCode: 400,
        body: JSON.stringify({
          error: "Error de validación",
          errors: errors,
        }),
      };

      return callback(null, response);
    }


    const responseData = await apiPeopleId(id);


    const response = {
      statusCode: 201,
      body: JSON.stringify({
        data: responseData,
      }),
    };

    return callback(null, response);

  } catch (ex) {

    return callback(null, ex);
  }

};
