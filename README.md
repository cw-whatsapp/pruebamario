
## Rutas de pruebas online



Api- Post man 
   
```json

  POST - https://pf3j6v2q83.execute-api.us-east-1.amazonaws.com/dev/registerUser
    body:
         {
            "nombre": "PEPE",
            "correo_electronico": "jmuspeda3ed@gmail.com",
            "contrasena": "Mario125@Lima",
            "fecha_creacion": "2024-02-26 00:15:07"
         }

  GET  - https://pf3j6v2q83.execute-api.us-east-1.amazonaws.com/dev/getUserAll

  POST - https://pf3j6v2q83.execute-api.us-east-1.amazonaws.com/dev/getUserId
    body:        
        {
            "id":16
        }

  GET  - https://pf3j6v2q83.execute-api.us-east-1.amazonaws.com/dev/getSwapiPeopleAll

  POST - https://pf3j6v2q83.execute-api.us-east-1.amazonaws.com/dev/getSwapiPeopleId
    body:
        {
            "id": 1
        }
```

## Uso



credenciales de AWS para serverles 
   
```c#

serverless config credentials --provider aws --key ID --secret SECRET_KEY -profile default
```

clonar proyecto 
   
```

git clone https://gitlab.com/cw-whatsapp/pruebamario.git
```

Instalar dependecias
   
```

npm install
```

revisar  `package.json`, para ver los comandos nesesarios 
   
```c#
npm run swagger // Mostrar documentacion swagger (localhost:3000/api-docs),
npm run test_all // Ejecutar todas las pruebas unitarias  dentro de la capeta __tests__
npm run invoke_register //Prueba local de registrar nuevo usuario en MySql
npm run invoke_userAll //Prueba local traer todos los usuarios
npm run invoke_userId //Prueba local traer un especifico usuario
npm run invoke_swapiPeopleAll // Prueba local de api swapi traducida a español (traer todos los registros)
npm run invoke_swapiPeopleId // Prueba local de api swapi traducida a español (tart un  registros)
npm run deploy //Publicar el proyecto lambda en AWS sin errores
npm run remove //Remover la publicacion en AWS
```


revisar  `.env`, para ver las variables 
   
```.env
DB_HOST="database-1.cfgyc4magfn7.us-east-1.rds.amazonaws.com"
DB_NAME="pruebabac"
DB_USER="admin"
DB_PASS="azcCKUHX1jiyiEhQdKRK"

SWAPI="https://swapi.py4e.com/api/"
```

