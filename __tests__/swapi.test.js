const { getSwapiPeopleAll, getSwapiPeopleId } = require('../handler');

describe('Función getSwapiPeopleId', () => {
    test('debería devolver la respuesta correcta cuando la entrada es válida', async () => {
        const mockEvent = {
            body: JSON.stringify({ id: 1 }),
        };
        const mockContext = {};
        const mockCallback = jest.fn();

        await getSwapiPeopleId(mockEvent, mockContext, mockCallback);

        expect(mockCallback).toHaveBeenCalledWith(null, {
            statusCode: 201,
            body: JSON.stringify({
                data: {

                    "nombre": "Luke Skywalker",
                    "altura": "172",
                    "masa": "77",
                    "color_de_cabello": "blond",
                    "color_de_piel": "fair",
                    "color_de_ojos": "blue",
                    "año_de_nacimiento": "19BBY",
                    "genero": "male",
                    "planeta_natal": "https://swapi.py4e.com/api/planets/1/",
                    "peliculas": [
                        "https://swapi.py4e.com/api/films/1/",
                        "https://swapi.py4e.com/api/films/2/",
                        "https://swapi.py4e.com/api/films/3/",
                        "https://swapi.py4e.com/api/films/6/",
                        "https://swapi.py4e.com/api/films/7/"
                    ],
                    "especies": [
                        "https://swapi.py4e.com/api/species/1/"
                    ],
                    "vehiculos": [
                        "https://swapi.py4e.com/api/vehicles/14/",
                        "https://swapi.py4e.com/api/vehicles/30/"
                    ],
                    "naves_estelares": [
                        "https://swapi.py4e.com/api/starships/12/",
                        "https://swapi.py4e.com/api/starships/22/"
                    ],
                    "creado": "2014-12-09T13:50:51.644000Z",
                    "editado": "2014-12-20T21:17:56.891000Z",
                    "url": "https://swapi.py4e.com/api/people/1/"
                }

            }),
        });
    });

    test('debería devolver un error de validación cuando el id es inválido', async () => {
        const mockEvent = {
            body: JSON.stringify({ id: '' }), // Se espera un id válido
        };
        const mockContext = {};
        const mockCallback = jest.fn();

        await getSwapiPeopleId(mockEvent, mockContext, mockCallback);

        expect(mockCallback).toHaveBeenCalledWith(null, {
            statusCode: 400,
            body: JSON.stringify({
                error: "Error de validación",
                errors: [
                    "El ID  no puede estar vacío."
                ],
            }),
        });
    });

    
});
